<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib\Doctrine;

/**
 * Trait LifecycleCallbacksTrait
 * @package DarCas\ZfAid\Stdlib\Doctrine
 *
 * @property \DateTime|null $insertDate
 * @property \DateTime|null $updateDate
 */
trait LifecycleCallbacksTrait
{
    /**
     * @param string|null $format null for \DateTime object
     *
     * @return \DateTime|null|string
     */
    public function getInsertDate($format = null)
    {
        if (is_null($this->insertDate)) {
            return null;
        } else {
            if (is_null($format)) {
                return $this->insertDate;
            } else {
                return $this->insertDate->format($format);
            }
        }
    }

    /**
     * @param string|null $format null for \DateTime object
     *
     * @return \DateTime|null|string
     */
    public function getUpdateDate($format = null)
    {
        if (is_null($this->updateDate)) {
            return null;
        } else {
            if (is_null($format)) {
                return $this->updateDate;
            } else {
                return $this->updateDate->format($format);
            }
        }
    }

    /**
     * {"ignore":"true"}
     * @return $this
     */
    public function unsetUpdateDate()
    {
        $this->updateDate = null;

        return $this;
    }

    /**
     * {"ignore":"true"}
     * To run when inserting a record
     */
    public function setDate($action)
    {
        if (in_array($action, ['insert', 'update'])) {
            /** @var string $property */
            $property = "{$action}Date";

            if (is_null($this->$property)) {
                $this->$property = new \DateTime('now');
            }
        }
    }
}
