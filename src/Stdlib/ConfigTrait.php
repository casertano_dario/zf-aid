<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

/**
 * Trait StdClassTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait ConfigTrait
{
    /**
     * @var \DarCas\ZfAid\Stdlib\ArrayUtils\DotNotation|null
     */
    protected $config = null;

    /**
     * @param string|null $path
     * @param mixed $default
     *
     * @return \DarCas\ZfAid\Stdlib\ArrayUtils\DotNotation|mixed
     */
    public function getAppConfig($path = null, $default = null)
    {
        if (is_null($this->config)) {
            /** @var array $config */
            $config = $this->getServiceManager()->get('Config');

            $this->config = new ArrayUtils\DotNotation($config);
        }

        if (is_null($path)) {
            return $this->config;
        } else {
            return $this->config->get($path, $default);
        }
    }
}
