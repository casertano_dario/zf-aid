<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use Zend\Http\Header\SetCookie;

/**
 * Trait CookieTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait CookieTrait
{
    /**
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getCookie($name, $default = null)
    {
        return isSet($_COOKIE[$name]) ? $_COOKIE[$name] : $default;
    }

    /**
     * @param string $name
     * @param string|null $value
     * @param string|null $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httpOnly
     *
     * @return \Zend\Http\Header\SetCookie
     * @throws \Zend\Http\Header\Exception\InvalidArgumentException
     */
    protected function setCookie(
        $name,
        $value = null,
        $expire = '+1 year',
        $path = '/',
        $domain = '',
        $secure = false,
        $httpOnly = true
    ) {
        /** @var SetCookie $setCookie */
        $setCookie = new SetCookie();
        $setCookie->setName($name)
            ->setValue($value)
            ->setPath($path)
            ->setDomain($domain)
            ->setSecure($secure)
            ->setHttponly($httpOnly);

        if (!is_null($expire)) {
            /** @var \DateTime $oExpires */
            $oExpires = new \DateTime('now');
            $oExpires->modify($expire);

            $setCookie->setExpires($oExpires);
        }

        return $setCookie;
    }

    /**
     * @param $name
     * @param string $path
     * @param string $domain
     *
     * @return \Zend\Http\Header\SetCookie
     * @throws \Zend\Http\Header\Exception\InvalidArgumentException
     */
    protected function unSetCookie($name, $path = '/', $domain = '')
    {
        return $this->setCookie($name, null, '-1 day', $path, $domain);
    }
}
