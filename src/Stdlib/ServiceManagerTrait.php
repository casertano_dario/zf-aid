<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use Interop\Container\ContainerInterface;

/**
 * Trait ServiceManagerTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait ServiceManagerTrait
{
    /**
     * @var \Interop\Container\ContainerInterface|\Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * {"ignore":"true"}
     * @param \Interop\Container\ContainerInterface $container
     *
     * @return $this
     */
    public function setServiceManager(ContainerInterface $container)
    {
        $this->serviceManager = $container;

        return $this;
    }

    /**
     * {"ignore":"true"}
     * @return \Zend\ServiceManager\ServiceManager
     */
    protected function getServiceManager()
    {
        return $this->serviceManager;
    }
}
