<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use DarCas\ZfAid\Exception\EntityException;
use DarCas\ZfAid\Helpers\StringHelper;
use Zend\Code\Reflection\DocBlockReflection;

/**
 * Trait StdClassTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait StdClassTrait
{
    /**
     * {"ignore":"true"}
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     * @throws \DarCas\ZfAid\Exception\EntityException
     */
    public function __call($name, $arguments)
    {
        if (preg_match('#^(set|get)([A-Z])(.*)$#', $name, $matches)) {
            /** @var string $property */
            $property = mb_strtolower($matches[2]) . $matches[3];
            if ($matches[1] == 'get') {
                return $this->__get($property);
            } elseif ($matches[1] == 'set') {
                $this->__set($property, $arguments[0]);

                return $this;
            }
        }
        throw new EntityException("Undefined method {$name}");
    }

    /**
     * {"ignore":"true"}
     * @param string $name
     *
     * @return mixed
     * @throws \DarCas\ZfAid\Exception\EntityException
     */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            if (is_string($this->$name) && StringHelper::isProbablyJson($this->$name)) {
                return json_decode($this->$name);
            }

            if (is_string($this->$name)) {
                if (mb_check_encoding($this->$name, 'UTF-8')) {
                    return $this->$name;
                } else {
                    return mb_convert_encoding($this->$name, 'UTF-8');
                }
            } else {
                return $this->$name;
            }
        } else {
            throw new EntityException("Undefined property {$name}");
        }
    }

    /**
     * {"ignore":"true"}
     * @param string $name
     * @param mixed $value
     *
     * @throws \DarCas\ZfAid\Exception\EntityException
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new EntityException("Undefined property {$name}");
        }
    }

    /**
     * {"ignore":"true"}
     * @return array
     * @throws \ReflectionException
     * @throws \Zend\Code\Reflection\Exception\InvalidArgumentException
     */
    protected function __toArray()
    {
        /** @var array $return */
        $return = [];
        /** @var string $class */
        $class = str_replace('DoctrineORMModule\Proxy\__CG__\\', '', get_class($this));
        /** @var \ReflectionClass $reflectionClass */
        $reflectionClass = new \ReflectionClass($class);
        /** @var \Zend\Code\Reflection\DocBlockReflection $docBlockReflection */
        $docBlockReflection = new DocBlockReflection($reflectionClass->getDocComment());

        /** @var \Zend\Code\Reflection\DocBlock\Tag\MethodTag $methodTag */
        foreach ($docBlockReflection->getTags('method') as $methodTag) {
            if (mb_substr($methodTag->getMethodName(), 0, 3) == 'get') {
                /** @var string $methodName */
                $methodName = mb_substr($methodTag->getMethodName(), 0, -2);
                /** @var string $varName */
                $varName = mb_strtolower(mb_substr($methodName, 3, 1)) . mb_substr($methodName, 4);

                $return[$varName] = $this->$methodName();
            }
        }

        /** @var \ReflectionMethod $reflectionMethod */
        foreach ($reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
            /** @var \Zend\Code\Reflection\DocBlockReflection $doc */
            $docBlockReflection = new DocBlockReflection($reflectionMethod->getDocComment());

            if (StringHelper::isProbablyJson($docBlockReflection->getLongDescription())) {
                /** @var \stdClass $conf */
                $conf = json_decode($docBlockReflection->getLongDescription());
            } elseif (StringHelper::isProbablyJson($docBlockReflection->getShortDescription())) {
                /** @var \stdClass $conf */
                $conf = json_decode($docBlockReflection->getShortDescription());
            }

            if (!$conf->ignore) {
                /** @var string $methodName */
                $methodName = $reflectionMethod->getName();
                /** @var string $varName */
                $varName = $conf->var ? : mb_strtolower(mb_substr($methodName, 3, 1)) . mb_substr($methodName, 4);

                $return[$varName] = $this->$methodName();
            }
        }

        return $return;
    }
}
