<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use Doctrine\ORM\EntityManager;

/**
 * Trait EntityManagerTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait EntityManagerTrait
{
    /**
     * @var \Doctrine\ORM\EntityManager[]
     */
    protected $entityManager;

    /**
     * {"ignore":"true"}
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param string $orm_name
     *
     * @return $this
     */
    public function setEntityManager(EntityManager $entityManager, $orm_name = 'orm_default')
    {
        $this->entityManager[$orm_name] = $entityManager;

        return $this;
    }

    /**
     * {"ignore":"true"}
     * @param string $orm_name
     *
     * @return \Doctrine\ORM\EntityManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \InvalidArgumentException
     */
    protected function getEntityManager($orm_name = 'orm_default')
    {
        if (!$this->entityManager[$orm_name]->isOpen()) {
            /** @var \Doctrine\DBAL\Connection $connection */
            $connection = $this->entityManager[$orm_name]->getConnection();
            /** @var \Doctrine\ORM\Configuration $config */
            $config = $this->entityManager[$orm_name]->getConfiguration();

            $this->entityManager[$orm_name] = $this->entityManager[$orm_name]::create($connection, $config);
        }

        return $this->entityManager[$orm_name];
    }
}
