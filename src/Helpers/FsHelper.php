<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2015-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */

namespace DarCas\ZfAid\Helpers;

use PDO;

/**
 * Class FsHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class FsHelper
{
    /**
     * Gets permissions for the given file as an octal value
     *
     * @param string $filename
     *
     * @return int
     */
    public static function fileperms($filename)
    {
        if (file_exists($filename)) {
            return substr(decoct(fileperms($filename)), 1);
        } else {
            return null;
        }
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    public static function filesize($filename)
    {
        if (file_exists($filename)) {
            /** @var int $size */
            $size = filesize($filename);
            /** @var array $units */
            $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
            /** @var int|float $power */
            $power = $size > 0 ? floor(log($size, 1024)) : 0;

            return number_format($size / pow(1024, $power), 2, '.', '') . ' ' . $units[$power];
        } else {
            return null;
        }
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    public static function filename_filter($filename)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $filename = pathinfo($filename, PATHINFO_FILENAME);
        $filename = preg_replace('#\s{2,}#is', '-', $filename);
        $filename = preg_replace('#\s{1}#is', '-', $filename);
        $filename = preg_replace('#[^A-Z0-9-]#is', '', $filename);
        $filename = mb_substr($filename, 0, 247 - mb_strlen($extension) - 1);

        return "{$filename}.{$extension}";
    }

    /**
     * @param string $pathname
     * @param int $mode
     * @param bool $recursive
     * @param bool $gitignore
     *
     * @return bool|string
     */
    public static function mkdir($pathname, $mode = 0755, $recursive = true, $gitignore = false)
    {
        if (!file_exists($pathname)) {
            mkdir($pathname, $mode, $recursive);
        } elseif (static::fileperms($pathname) != '0' . decoct($mode)) {
            chmod($pathname, $mode);
        }

        if ($gitignore && !file_exists("{$pathname}/.gitignore")) {
            file_put_contents("{$pathname}/.gitignore", '*' . PHP_EOL . '!.gitignore');
            chmod("{$pathname}/.gitignore", 0644);
        }

        return realpath($pathname);
    }

    /**
     * @param string $pathname
     * @param bool $recursive
     *
     * @return bool
     */
    public static function rmdir($pathname, $recursive = true)
    {
        if ($recursive === false) {
            return rmdir($pathname);
        } else {
            /** @var array $files */
            $files = array_diff(scandir($pathname), ['.', '..']);

            /** @var string $file */
            foreach ($files as $file) {
                if (is_dir("{$pathname}/{$file}")) {
                    self::mkdir("{$pathname}/{$file}");
                } else {
                    unlink("{$pathname}/{$file}");
                }
            }

            return rmdir($pathname);
        }
    }

    /**
     * @param string $filename
     * @param int $fetch PDO::FETCH_NUM | PDO::FETCH_ASSOC | PDO::FETCH_BOTH
     * @param array $opts
     *
     * @return array
     */
    public static function parse_csv($filename, $fetch = PDO::FETCH_ASSOC, array $opts = array())
    {
        if (file_exists($filename)) {
            /** @var array $options */
            $options = array_merge([
                'delimiter' => ',',
                'enclosure' => '"',
                'escape' => "\\",
            ], $opts);

            /** @var array $raw */
            $raw = array_map(function ($item) use ($options) {
                return str_getcsv($item, $options['delimiter'], $options['enclosure'], $options['escape']);
            }, file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));

            if (count($raw)) {
                /** @var array $cols */
                $cols = array_shift($raw);
                $cols = array_map('trim', $cols);
                $cols = array_map('mb_strtolower', $cols);

                for ($i = 0, $ii = count($raw), $recordset = []; $i != $ii; $i++) {
                    /** @var array $rowSet */
                    $rowSet = [];
                    /** @var array $row */
                    $row = $raw[$i];

                    if ($fetch == PDO::FETCH_NUM) {
                        foreach ($row as $key => $value) {
                            $rowSet[$key] = trim($value);
                        }
                    } elseif ($fetch == PDO::FETCH_ASSOC) {
                        foreach ($row as $key => $value) {
                            $rowSet[$cols[$key]] = trim($value);
                        }
                    } elseif ($fetch == PDO::FETCH_BOTH) {
                        foreach ($row as $key => $value) {
                            $rowSet[$key] = trim($value);
                            $rowSet[$cols[$key]] = trim($value);
                        }
                    }

                    $recordset[] = $rowSet;
                }

                return $recordset;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
}
