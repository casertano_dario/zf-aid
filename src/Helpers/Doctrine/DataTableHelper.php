<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */

namespace DarCas\ZfAid\Helpers\Doctrine;

use Zend\Http\PhpEnvironment\Request;

/**
 * Class DataTableHelper
 * @package DarCas\ZfAid\Helpers\Ajax
 */
final class DataTableHelper
{
    /**
     * @var \Zend\Stdlib\ParametersInterface
     */
    private $params = null;
    /**
     * @var array
     */
    private $columns;
    /**
     * @var array
     */
    private $order;
    /**
     * @var array
     */
    private $search;
    /**
     * @var int
     */
    private $limit;
    /**
     * @var int
     */
    private $offset;

    /**
     * @param \Zend\Http\PhpEnvironment\Request $request
     */
    public function __construct(Request $request)
    {
        if ($request->isGet()) {
            $this->params = $request->getQuery();
        } elseif ($request->isPost()) {
            $this->params = $request->getPost();
        }

        $this->columns = $this->getParam('columns');
        $this->order = $this->getParam('order');
        $this->search = $this->getParam('search');
        $this->limit = $this->getParam('length');
        $this->offset = $this->getParam('start');
    }

    /**
     * @param string|null $name
     * @param mixed|null $default
     *
     * @return \Zend\Stdlib\ParametersInterface|mixed
     */
    public function getParam($name = null, $default = null)
    {
        return $this->params->get($name, $default);
    }

    /**
     * @param int|null $default
     *
     * @return int
     */
    public function getLimit($default = null)
    {
        if (is_null($this->limit)) {
            return $default;
        } else {
            if ($this->limit == -1) {
                return null;
            } else {
                return (int) $this->limit;
            }
        }
    }

    /**
     * @param int|null $default
     *
     * @return int
     */
    public function getOffset($default = null)
    {
        return is_null($this->offset) ? $default : (int) $this->offset;
    }

    /**
     * @param mixed $default
     *
     * @return mixed|null
     */
    public function getSearch($default = null)
    {
        return empty($this->search['value']) ? $default : $this->search['value'];
    }

    /**
     * @param string|null $aliasTable
     * @param array|null $default
     *
     * @return array|null
     */
    public function getOrder($aliasTable = null, array $default = null)
    {
        if (!empty($this->columns[$this->order[0]['column']]['name'])) {
            /** @var string $column */
            $column = $this->columns[$this->order[0]['column']]['name'];
        } elseif (!empty($this->columns[$this->order[0]['column']]['data'])) {
            /** @var string $column */
            $column = $this->columns[$this->order[0]['column']]['data'];
        } else {
            return $default;
        }

        if (strstr($column, '.') !== false) {
            return [$column, mb_strtoupper($this->order[0]['dir'])];
        } elseif (!is_null($aliasTable)) {
            return ["{$aliasTable}.{$column}", mb_strtoupper($this->order[0]['dir'])];
        } else {
            return [$column, mb_strtoupper($this->order[0]['dir'])];
        }
    }
}
