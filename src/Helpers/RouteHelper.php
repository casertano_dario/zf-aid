<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */

namespace DarCas\ZfAid\Helpers;

use Zend\Router\Http;

/**
 * Class RouteHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class RouteHelper
{
    /**
     * Simple CRUD implementation
     *
     * @param string $single_id_name
     * @param array|null $tools
     *
     * @return array
     */
    public static function crud($single_id_name = 'id', array $tools = null)
    {
        /** @var array $route */
        $route = [
            'create' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/create',
                    'defaults' => [
                        'action' => 'form',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'create' => [
                        'type' => Http\Literal::class,
                        'options' => [
                            'route' => '/create',
                            'defaults' => [
                                'action' => 'create',
                            ],
                        ],
                    ],
                ],
            ],
            'single' => [
                'type' => Http\Segment::class,
                'options' => [
                    'route' => "/:{$single_id_name}",
                    'constraints' => [
                        $single_id_name => '[0-9|,]+',
                    ],
                    'defaults' => [
                        'action' => 'form',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'update' => [
                        'type' => Http\Literal::class,
                        'options' => [
                            'route' => '/update',
                            'defaults' => [
                                'action' => 'update',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'status' => [
                                'type' => Http\Segment::class,
                                'options' => [
                                    'route' => '/status/:status',
                                    'constraints' => [
                                        'status' => '[a-zA-Z_-]+',
                                    ],
                                    'defaults' => [
                                        'action' => 'set-status',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Http\Literal::class,
                        'options' => [
                            'route' => '/delete',
                            'defaults' => [
                                'action' => 'delete',
                            ],
                        ],
                    ],
                ],
            ],
            'trash' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/trash',
                    'defaults' => [
                        'action' => 'trash-can',
                    ],
                ],
            ],
        ];

        if (!is_null($tools)) {
            $route['tools'] = [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/tools',
                ],
                'child_routes' => $tools,
            ];
        }

        return $route;
    }

    /**
     * CRUD implementation with REST architecture
     *
     * @param string $single_id_name
     *
     * @return array
     */
    public static function rest($single_id_name = 'id')
    {
        /** @var array $route */
        $route = [
            'create' => [
                'type' => Http\Method::class,
                'options' => [
                    'verb' => 'post',
                    'defaults' => [
                        'action' => 'post',
                    ],
                ],
            ],
            'read' => [
                'type' => Http\Method::class,
                'options' => [
                    'verb' => 'get',
                    'defaults' => [
                        'action' => 'get',
                    ],
                ],
            ],
            'single' => [
                'type' => Http\Segment::class,
                'options' => [
                    'route' => "/:{$single_id_name}",
                    'constraints' => [
                        $single_id_name => '[0-9|,]+',
                    ],
                ],
                'child_routes' => [
                    'read' => [
                        'type' => Http\Method::class,
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'action' => 'get',
                            ],
                        ],
                    ],
                    'update' => [
                        'type' => Http\Method::class,
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'action' => 'put',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'status' => [
                                'type' => Http\Segment::class,
                                'options' => [
                                    'route' => '/:status',
                                    'constraints' => [
                                        'status' => '[a-zA-Z_-]+',
                                    ],
                                    'defaults' => [
                                        'action' => 'set-status',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Http\Method::class,
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'action' => 'delete',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $route;
    }
}
