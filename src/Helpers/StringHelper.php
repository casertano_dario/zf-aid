<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */

namespace DarCas\ZfAid\Helpers;

/**
 * Class StringHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class StringHelper
{
    /**
     * @param string $string
     *
     * @return bool
     */
    public static function isProbablyJson($string)
    {
        $json = json_decode($string);

        if (is_object($json) || is_array($json)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function CamelCaseToUnderscore($string)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = ($match == mb_strtoupper($match)) ? mb_strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }
}
