<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */

namespace DarCas\ZfAid\Helpers;

/**
 * Class VarHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class VarHelper
{
    /**
     * @param mixed $var
     *
     * @return string
     */
    public static function getType($var)
    {
        if (is_object($var)) {
            return get_class($var);
        } else {
            return gettype($var);
        }
    }
}
