<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */

namespace DarCas\ZfAid\Helpers;

/**
 * Class ArrayHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class ArrayHelper
{
    /**
     * Merge two or more arrays recursively
     *
     * @param array $array [, array $...]
     *
     * @return array
     */
    public static function mergeRecursiveDistinct(array $array)
    {
        $arrays = func_get_args();
        $base = array_shift($arrays);
        if (!is_array($base)) {
            $base = empty($base) ? [] : array($base);
        }
        foreach ($arrays as $append) {
            if (!is_array($append)) {
                $append = array($append);
            }
            foreach ($append as $key => $value) {
                if (!array_key_exists($key, $base) && !is_numeric($key)) {
                    $base[$key] = $append[$key];
                    continue;
                }
                if (is_array($value) || is_array($base[$key])) {
                    $base[$key] = static::mergeRecursiveDistinct($base[$key], $append[$key]);
                } elseif (is_numeric($key)) {
                    if (!in_array($value, $base)) {
                        $base[] = $value;
                    }
                } else {
                    $base[$key] = $value;
                }
            }
        }

        return $base;
    }

    /**
     * Unset key with null value
     *
     * @param array $array
     *
     * @return array
     */
    public static function cleanEmptyKeys(array $array)
    {
        if (!is_array($array)) {
            return $array;
        } else {
            foreach ($array as $key => $value) {
                if (is_array($value) && count($value)) {
                    $r_array[$key] = static::cleanEmptyKeys($value);
                } elseif (is_numeric($value)) {
                    if (strpos($value, '.') !== false) {
                        list($units, $decimals) = explode('.', $value);
                        $units = (int)$units;
                        $decimals = (int)$decimals;

                        if ($units || $decimals) {
                            $r_array[$key] = $value;
                        }
                    } elseif ($value) {
                        $r_array[$key] = $value;
                    }
                } elseif (is_string($value) && trim($value)) {
                    $r_array[$key] = $value;
                }
            }

            return isSet($r_array) ? $r_array : [];
        }
    }

    /**
     * @param array $array
     * @param array $keys
     * @param bool $preserveKey
     *
     * @return array
     */
    public static function sliceByKeys(array $array, array $keys, $preserveKey = true) {
        /** @var array $return */
        $return = [];

        /** @var mixed $key */
        foreach ($keys as $key) {
            if (array_key_exists($key, $array)) {
                if ($preserveKey) {
                    $return[$key] = $array[$key];
                } else {
                    $return[] = $array[$key];
                }
            }
        }

        return $return;
    }
}
