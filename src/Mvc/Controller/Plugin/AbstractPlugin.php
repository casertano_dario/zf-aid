<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\Controller\Plugin;

use DarCas\ZfAid\Stdlib\ServiceManagerTrait;
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin AS ZAbstractPlugin;

/**
 * Class AbstractPlugin
 * @package DarCas\ZfAid\Mvc\Controller\Plugin
 */
abstract class AbstractPlugin extends ZAbstractPlugin
{
    use ServiceManagerTrait;

    /**
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setServiceManager($container);
    }
}
