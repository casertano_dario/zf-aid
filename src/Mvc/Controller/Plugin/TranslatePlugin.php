<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class TranslatePlugin
 * @package DarCas\ZfAid\Mvc\Controller\Plugin
 */
final class TranslatePlugin extends AbstractPlugin
{
    /**
     * @var \Zend\I18n\Translator\Translator
     */
    private $translator;

    /**
     * @param \Interop\Container\ContainerInterface $container
     *
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(\Interop\Container\ContainerInterface $container)
    {
        if (!$container->has('translator')) {
            throw new \Exception("Zend I18n Translator not configured");
        }

        $this->translator = $container->get('translator');
    }

    /**
     * @param string $message
     * @param string $textDomain
     * @param string $locale
     *
     * @return string
     */
    public function __invoke($message = null, $textDomain = null, $locale = null)
    {
        if (is_null($message)) {
            return $this->translator;
        } else {
            return $this->translator->translate($message, $textDomain, $locale);
        }
    }

    /**
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call($method, array $arguments = [])
    {
        if (!method_exists($this->translator, $method)) {
            throw new \Exception("The method «{$method}» does not exist");
        }

        return call_user_func_array([$this->translator, $method], $arguments);
    }
}
