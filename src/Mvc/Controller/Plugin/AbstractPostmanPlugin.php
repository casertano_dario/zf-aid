<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\Controller\Plugin;

use DarCas\ZfAid\Stdlib;
use Zend\Mail;
use Zend\View;

/**
 * Class PostmanPlugin
 * @package Admin\Services
 */
abstract class AbstractPostmanPlugin extends AbstractPlugin
{
    use Stdlib\ConfigTrait;
    /**
     * @return $this
     */
    public function __invoke()
    {
        $this->getTransport();

        return $this;
    }

    /**
     * @return \Zend\Mail\Transport\TransportInterface
     */
    protected function getTransport()
    {
        /** @var string $transport */
        $transport = $this->getAppConfig()->get('postman.transport.class');

        switch ($transport) {
            case Mail\Transport\Smtp::class:
                /** @var \Zend\Mail\Transport\SmtpOptions $options */
                $options = new Mail\Transport\SmtpOptions($this->getAppConfig()->get('postman.transport.options'));

                /** @var \Zend\Mail\Transport\Smtp $class */
                $class = new $transport();
                $class->setOptions($options);

                return $class;
            case Mail\Transport\Sendmail::class:
            default:
                return new $transport();
        }
    }

    /**
     * @return \Zend\View\Renderer\PhpRenderer
     */
    protected function getPhpRenderer()
    {
        return $this->getServiceManager()->get('Zend\View\Renderer\RendererInterface');
    }

    /**
     * @return \Zend\View\Resolver\TemplateMapResolver|\Zend\View\Resolver\TemplatePathStack
     * @throws \Zend\View\Exception\InvalidArgumentException
     */
    protected function getResolver()
    {
        /** @var \Zend\View\Resolver\TemplatePathStack $resolver */
        $resolver = new View\Resolver\TemplatePathStack();
        $resolver->setPaths($this->getAppConfig()->get('view_manager.template_path_stack'));

        return $resolver;
    }

    /**
     * @param \Zend\View\Model\ViewModel $viewModel
     * @param string $subject
     *
     * @return string
     * @throws \Zend\View\Exception\DomainException
     * @throws \Zend\View\Exception\InvalidArgumentException
     * @throws \Zend\View\Exception\RuntimeException
     */
    protected function getLayout($viewModel, $subject)
    {
        /** @var string $content */
        $content = $this->getPhpRenderer()->render($viewModel);

        /** @var \Zend\View\Model\ViewModel $viewLayout */
        $viewLayout = new View\Model\ViewModel();
        $viewLayout->setTemplate('postman');
        $viewLayout->setVariable('subject', $subject);
        $viewLayout->setVariable($viewModel->captureTo(), $content);

        return $this->getPhpRenderer()->render($viewLayout);
    }
}
