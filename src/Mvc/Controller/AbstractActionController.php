<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\Controller;

use DarCas\ZfAid\Stdlib;
use Zend\Mvc\Controller as ZendController;

/**
 * Class AbstractActionController
 * @package DarCas\ZfAid\Mvc\Controller
 */
abstract class AbstractActionController extends ZendController\AbstractActionController
{
    use Stdlib\ConfigTrait;
    use Stdlib\EntityManagerTrait;
    use Stdlib\ServiceManagerTrait;

    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param array|null $options
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(\Interop\Container\ContainerInterface $container, array $options = null)
    {
        $this->setEntityManager($container->get('doctrine.entitymanager.orm_default'));
        $this->setServiceManager($container);
    }

    /**
     * Return all route parameters or a single route parameter.
     *
     * @param string $param
     * @param mixed $default
     *
     * @return mixed
     * @throws \Zend\Mvc\Exception\RuntimeException
     */
    public function fromRoute($param = null, $default = null)
    {
        return $this->params()->fromRoute($param, $default);
    }
}
