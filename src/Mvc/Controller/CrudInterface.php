<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\Controller;

/**
 * Interface CrudInterface
 * @package DarCas\ZfAid\Mvc\Controller
 */
interface CrudInterface
{
    /**
     * GET-method
     */
    public function getAction();
    /**
     * POST-method
     */
    public function postAction();
    /**
     * PUT-method
     */
    public function putAction();
    /**
     * DELETE-method
     */
    public function deleteAction();
    /**
     * OPTIONS-method
     */
    public function optionsAction();
}
