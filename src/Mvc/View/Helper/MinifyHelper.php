<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\View\Helper;

use DarCas\ZfAid\Stdlib;
use GK\JavascriptPacker;
use tubalmartin\CssMin\Minifier;
use Zend\View\Exception;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Helper\Placeholder;

/**
 * Class MinifyHelper
 * @package DarCas\ZfAid\Mvc\View\Helper
 */
class MinifyHelper extends AbstractHelper
{
    use Stdlib\ConfigTrait;
    use Stdlib\ServiceManagerTrait {
        getServiceManager as protected;
    }
    private $captureLock = false;
    private $captureType = null;
    private $captureScriptType = null;
    private $captureScriptAttrs = null;

    /**
     * @param \Interop\Container\ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(\Interop\Container\ContainerInterface $container)
    {
        $this->setServiceManager($container);
    }

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param string $script
     *
     * @return string
     */
    public function js($script)
    {
        if ($this->getAppConfig()->get('minify.js')) {
            /** @var \GK\JavascriptPacker $packer */
            $packer = new JavascriptPacker($script, 'Normal', true, false);

            return $packer->pack();
        } else {
            return $script;
        }
    }

    /**
     * @param string $style
     *
     * @return string
     */
    public function css($style)
    {
        if ($this->getAppConfig()->get('minify.css')) {
            /** @var \tubalmartin\CssMin\Minifier $css */
            $css = new Minifier();
            $css->keepSourceMapComment();

            return $css->run($style);
        } else {
            return $style;
        }
    }

    /**
     * @param string $captureType
     * @param string $type
     * @param array $attrs
     *
     * @throws \Zend\View\Exception\RuntimeException
     */
    public function jsStartCapture(
        $captureType = Placeholder\Container\AbstractContainer::APPEND,
        $type = 'text/javascript',
        $attrs = []
    ) {
        if ($this->captureLock) {
            throw new Exception\RuntimeException('Cannot nest headScript captures');
        }

        $this->captureLock = true;
        $this->captureType = $captureType;
        $this->captureScriptType = $type;
        $this->captureScriptAttrs = $attrs;
        ob_start();
    }

    /**
     * @throws \Exception
     */
    public function jsStopCapture()
    {
        /** @var string $content */
        $content = ob_get_clean();
        $type = $this->captureScriptType;
        $attrs = $this->captureScriptAttrs;
        $this->captureScriptType = null;
        $this->captureScriptAttrs = null;
        $this->captureLock = false;

        switch ($this->captureType) {
            case Placeholder\Container\AbstractContainer::SET:
            case Placeholder\Container\AbstractContainer::PREPEND:
            case Placeholder\Container\AbstractContainer::APPEND:
                $action = strtolower($this->captureType) . 'Script';
                break;
            default:
                $action = 'appendScript';
                break;
        }

        $this->headScript()->$action($this->js($content), $type, $attrs);
    }

    /**
     * @return \Zend\View\Helper\HeadScript
     */
    protected function headScript()
    {
        return $this->getServiceManager()->get('ViewHelperManager')->get('HeadScript');
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Request
     */
    protected function getRequest()
    {
        /** @var \Zend\Http\PhpEnvironment\Request $request */
        $request = $this->getServiceManager()->get('Request');

        return $request;
    }
}
