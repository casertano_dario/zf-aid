<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Console\Controller;

use DarCas\ZfAid\Stdlib;
use Interop\Container\ContainerInterface;
use Zend\Console\Adapter\AdapterInterface;
use Zend\Console\ColorInterface;
use ZF\Console\Route;

/**
 * Class AbstractActionController
 * @package DarCas\ZfAid\Console\Controller
 */
abstract class AbstractActionController implements AbstractActionControllerInterface
{
    use Stdlib\ServiceManagerTrait;
    /**
     * @var \Zend\Console\Adapter\AdapterInterface
     */
    protected $console;
    /**
     * @var \ZF\Console\Route
     */
    protected $route;

    /**
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setServiceManager($container);
    }

    /**
     * @param \ZF\Console\Route $route
     * @param \Zend\Console\Adapter\AdapterInterface $console
     *
     * @return mixed
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        $this->console = $console;
        $this->route = $route;

        /** @var string $action */
        $action = $route->getMatchedParam('action', 'not-found');
        /** @var string $method */
        $method = static::getMethodFromAction($action);

        if (!method_exists($this, $method)) {
            $method = 'notFoundAction';
        }

        return $this->$method();
    }

    /**
     * @param array $header
     * @param array $dataTable
     */
    public function createTable(array $header, array $dataTable)
    {
        /** @var array $keys */
        $keys = array_keys($header);
        /** @var array $colSizes */
        $colSizes = array_map(function ($v) {
            return mb_strlen($v);
        }, $header);

        /** @var array $recordSet */
        foreach ($dataTable as $recordSet) {
            /**
             * @var string $key
             * @var string $v
             */
            foreach ($recordSet as $key => $v) {
                /** @var int $len */
                $len = mb_strlen($v);

                if ($colSizes[$key] < $len) {
                    $colSizes[$key] = $len;
                }
            }
        }

        # HEADER
        $this->getConsole()->write('┌');

        foreach ($header as $k => $col) {
            if ($k != $keys[0]) {
                $this->getConsole()->write('┬');
            }

            $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
        }

        $this->getConsole()->writeLine('┐');

        foreach ($header as $k => $col) {
            if ($k == $keys[0]) {
                $this->getConsole()->write('│');
            }

            $this->getConsole()->write(' ');
            $this->getConsole()->write(str_pad($col, $colSizes[$k], ' ', STR_PAD_RIGHT), ColorInterface::MAGENTA);
            $this->getConsole()->write(' ');
            $this->getConsole()->write('│');
        }

        $this->getConsole()->writeLine();

        if (count($dataTable)) {
            /** @var array $recordSet */
            foreach ($dataTable as $recordSet) {
                $this->getConsole()->write('├');

                foreach ($header as $k => $col) {
                    if ($k != $keys[0]) {
                        $this->getConsole()->write('┼');
                    }

                    $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
                }

                $this->getConsole()->writeLine('┤');

                foreach ($recordSet as $k => $v) {
                    if ($k == $keys[0]) {
                        $this->getConsole()->write('│');
                    }

                    if (is_numeric($v)) {
                        $pad_type = STR_PAD_LEFT;
                    } else {
                        $pad_type = STR_PAD_RIGHT;
                    }

                    $this->getConsole()->write(' ');
                    $this->getConsole()->write(str_pad($v, $colSizes[$k], ' ', $pad_type), ColorInterface::LIGHT_WHITE);
                    $this->getConsole()->write(' ');
                    $this->getConsole()->write('│');
                }

                $this->getConsole()->writeLine('');
            }
        } else {
            $this->getConsole()->write('│');
            $this->getConsole()->write(str_pad('No data available in table',
                array_sum(array_values($colSizes)) + count($colSizes) + (count($colSizes) * 2) - 1, ' ', STR_PAD_BOTH),
                ColorInterface::LIGHT_RED);
            $this->getConsole()->writeLine('│');
        }

        # FOOTER
        $this->getConsole()->write('└');

        foreach ($header as $k => $col) {
            if ($k != $keys[0]) {
                $this->getConsole()->write('┴');
            }

            $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
        }

        $this->getConsole()->writeLine('┘');
    }

    /**
     * @return void
     */
    public function notFoundAction()
    {
        /** @var \Zend\Console\Adapter\AdapterInterface $console */
        $console = $this->getConsole();
        $console->writeLine('Action not found', ColorInterface::RED);
        $console->writeLine();
    }

    /**
     * Transform an "action" token into a method name
     *
     * @param  string $action
     *
     * @return string
     */
    public static function getMethodFromAction($action)
    {
        $method = str_replace(['.', '-', '_'], ' ', $action);
        $method = ucwords($method);
        $method = str_replace(' ', '', $method);
        $method = lcfirst($method);
        $method .= 'Action';

        return $method;
    }

    /**
     * @return \Zend\Console\Adapter\AdapterInterface
     */
    protected function getConsole()
    {
        return $this->console;
    }

    /**
     * @return \ZF\Console\Route
     */
    protected function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $param
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getParam($param, $default = null)
    {
        return $this->getRoute()->getMatchedParam($param, $default);
    }
}
