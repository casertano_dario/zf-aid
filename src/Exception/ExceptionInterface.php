<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Exception;

/**
 * Interface ExceptionInterface
 * @package DarCas\ZfAid\Exception
 */
interface ExceptionInterface
{}
