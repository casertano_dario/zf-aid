<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Form;

use Zend\Form\Form AS ZForm;
use Zend\Stdlib\Parameters;

/**
 * Class AbstractForm
 * @package Api\Form
 */
abstract class AbstractForm extends ZForm
{
    /**
     * @var null|\Zend\Stdlib\Parameters
     */
    protected $dataValidated = null;

    /**
     * @param string|null $name
     * @param mixed|null $default
     *
     * @return mixed|\Zend\Stdlib\Parameters
     * @throws \Zend\Form\Exception\DomainException
     */
    public function getFormData($name = null, $default = null)
    {
        /** @var array $validated */
        $validated = $this->getData();
        /** @var array $raw */
        $raw = $this->data;

        if (is_null($this->dataValidated)) {
            $this->dataValidated = new Parameters(array_merge($raw, $validated));
        }

        if (is_null($name)) {
            return $this->dataValidated;
        } else {
            return $this->dataValidated->get($name) ?: $default;
        }
    }
}
