<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid;

/**
 * Class Debug
 * @package DarCas\ZfAid\Debug
 */
class Debug extends \Zend\Debug\Debug
{
    /**
     * @param mixed $var [, mixed $... ]
     */
    public static function print_r(...$var)
    {
        $print_r = \print_r((func_num_args() == 1) ? func_get_arg(0) : $var, true);
        $return = [];
        if (static::getSapi() == 'cli') {
            $return[] = PHP_EOL;
            $return[] = $print_r;
            $return[] = PHP_EOL;
        } else {
            $return[] = '<pre>';
            $return[] = htmlentities($print_r);
        }
        echo implode(PHP_EOL, $return);
        exit;
    }
}
