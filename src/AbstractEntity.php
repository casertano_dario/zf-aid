<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2018 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid;

use DarCas\ZfAid\Stdlib;

/**
 * Class AbstractEntity
 * @package DarCas\ZfAid\Entity
 */
abstract class AbstractEntity
{
    use Stdlib\EntityManagerTrait {
        getEntityManager as public;
    }
    use Stdlib\ServiceManagerTrait {
        getServiceManager as public;
    }
    use Stdlib\StdClassTrait;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;
    const STATUS_TRASH = -1;
}
